package types

type OtrsCreateTicketRequest struct {
	Ticket       OtrsRequestTicket     `json:"Ticket"`
	Article      OtrsRequestArticle    `json:"Article"`
	UserLogin    string                `json:"UserLogin"`
	Password     string                `json:"Password"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField"`
	Attachment   []OtrsAttachment      `json:"Attachments,omitempty"`
}

type OtrsUpdateTicketRequest struct {
	Ticket       OtrsRequestTicket     `json:"Ticket"`
	Article      OtrsRequestArticle    `json:"Article"`
	UserLogin    string                `json:"UserLogin"`
	Password     string                `json:"Password"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField"`
	Attachment   []OtrsAttachment      `json:"Attachment,omitempty"`
}

type OtrsTicketSearchRequest struct {
	UserLogin    string                 `json:"UserLogin"`
	Password     string                 `json:"Password"`
	DynamicField OtrsDynamicFieldSearch `json:"DynamicField"`
}

type OtrsTicketGetRequest struct {
	UserLogin     string `json:"UserLogin"`
	Password      string `json:"Password"`
	AllArticles   int8   `json:"AllArticles"`
	DynamicFields int8   `json:"DynamicFields"`
	TicketID      string `json:"TicketID"`
}

type OtrsUpdateRequest struct {
	UserLogin    string                `json:"UserLogin"`
	Password     string                `json:"Password"`
	TicketID     string                `json:"TicketID"`
	Ticket       OtrsRequestTicket     `json:"Ticket"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField, omitempty"`
}

type OtrsAddReplyRequest struct {
	UserLogin    string                `json:"UserLogin"`
	Password     string                `json:"Password"`
	TicketID     string                `json:"TicketID"`
	From         string                `json:"From"`
	To           string                `json:"To"`
	Subject      string                `json:"Subject"`
	Body         string                `json:"Body"`
	Visible      int                   `json:"IsVisibleForCustomer"`
	Cc           string                `json:"Cc"`
	Attachments  []OtrsAttachment      `json:"Attachments,omitempty"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField,omitempty"`
}

type OtrsUpdateArticleRequest struct {
	UserLogin    string                `json:"UserLogin"`
	Password     string                `json:"Password"`
	TicketID     string                `json:"TicketID"`
	ArticleID    string                `json:"ArticleID"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField,omitempty"`
}

type OtrsRequestTicket struct {
	Title        string `json:"Title, omitempty"`
	Queue        string `json:"Queue, omitempty"`
	Lock         string `json:"Lock, omitempty"`
	Type         string `json:"Type, omitempty"`
	State        string `json:"State, omitempty"`
	Priority     string `json:"Priority, omitempty"`
	Owner        string `json:"Owner, omitempty"`
	CustomerID   string `json:"CustomerID, omitempty"`
	CustomerUser string `json:"CustomerUser, omitempty"`
}

type OtrsRequestArticle struct {
	From                 string `json:"From"`
	To                   string `json:"To"`
	ContentType          string `json:"ContentType"`
	CommunicationChannel string `json:"CommunicationChannel"`
	SenderType           string `json:"SenderType"`
	Subject              string `json:"Subject"`
	Body                 string `json:"Body"`
	IsVisibleForCustomer string `json:"IsVisibleForCustomer"`
	Cc                   string `json:"Cc"`
}

type OtrsDynamicFieldAdd struct {
	Name  string `json:"Name"`
	Value string `json:"Value"`
}

type OtrsDynamicFieldSearch struct {
	Name   string `json:"Name"`
	Equals int64  `json:"Equals,string"`
}

type OtrsReplyHook struct {
	TicketID      string           `json:"TicketID"`
	ArticleID     string           `json:"ArticleID"`
	FDTicketID    string           `json:"FreshDeskTicketID"`
	FDDomain      string           `json:"FreshDeskDomain"`
	Body          string           `json:"Body"`
	To            string           `json:"To"`
	Cc            string           `json:"Cc"`
	Attachments   []OtrsAttachment `json:"Attachments"`
	CustomerEmail string
}

type OtrsSearchTicketResponse struct {
	ArticleID    string `json:"ArticleID"`
	TicketNumber string `json:"TicketNumber"`
	TicketID     string `json:"TicketID"`
}

type OtrsAttachment struct {
	Content     string `json:"Content"`
	ContentType string `json:"ContentType"`
	Filename    string `json:"Filename"`
}

type OtrsGetTicketResponse struct {
	Ticket []OtrsTicket `json:"Ticket"`
}

type OtrsTicket struct {
	TicketID string        `json:"TicketID"`
	Article  []OtrsArticle `json:"Article"`
}

type OtrsArticle struct {
	ArticleID    string                `json:"ArticleID"`
	From         string                `json:"From"`
	DynamicField []OtrsDynamicFieldAdd `json:"DynamicField"`
}

type OtrsUpdateTicketHook struct {
	TicketID   string `json:"TicketID"`
	FDTicketID int64  `json:"FreshDeskTicketID,string"`
	FDDomain   string `json:"FreshDeskDomain"`
	Priority   string `json:"Priority"`
	State      string `json:"State"`
}
