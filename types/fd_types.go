package types

type FDTicket struct {
	CcEmails []string `json:"cc_emails"`
	//FwdEmails []string `json:"fwd_emails"`
	ReplyCcEmails []string `json:"reply_cc_emails"`
	//FrEscalated bool `json:"fr_escalated"`
	//Spam bool `json:"spam"`
	//EmailConfigId int64 `json:"email_config_id"`
	//GroupId int64 `json:"group_id"`
	Priority    int8  `json:"priority"`
	RequesterId int64 `json:"requester_id"`
	ResponderId int64 `json:"responder_id"`
	//Source                 int8                  `json:"source"`
	//CompanyId              int64                 `json:"company_id"`
	Status  int8   `json:"status"`
	Subject string `json:"subject"`
	//AssociationType        int8                  `json:"association_type"`
	ToEmails []string `json:"to_emails"`
	//ProductId              int64                 `json:"product_id"`
	Id   int64  `json:"id"`
	Type string `json:"type"`
	//DueBy                  string                `json:"due_by"`
	//FrDueBy                string                `json:"fr_due_by"`
	//IsEscalated            bool                  `json:"is_escalated"`
	//CustomFields           FDCustomFields `json:"custom_fields"`
	//CreatedAt              string                `json:"created_at"`
	//UpdatedAt              string                `json:"updated_at"`
	//AssociatedTicketsCount int64                 `json:"associated_tickets_count"`
	//Tags                   []string              `json:"tags"`
	Description     string         `json:"description"`
	DescriptionText string         `json:"description_text"`
	Attachments     []FDAttachment `json:"attachments"`
}

type FDCustomFields struct {
	OfficeLocation string `json:"office_location"`
}

type FDAttachment struct {
	ContentType string `json:"content_type"`
	URL         string `json:"attachment_url"`
	Name        string `json:"name"`
}

type FDContact struct {
	Email string `json:"email"`
}

type FDTicketHook struct {
	ID             int64  `json:"ID"`
	From           string `json:"From"`
	To             string `json:"To"`
	OfficeLocation string `json:"OfficeLocation"`
	Domain         string `json:"Domain"`
}

type FDReplyHook struct {
	ID             int64  `json:"ID"`
	Reply          string `json:"Reply"`
	From           string `json:"From"`
	To             string `json:"To"`
	Status         string `json:"Status"`
	Priority       string `json:"Priority"`
	OfficeLocation string `json:"OfficeLocation"`
	Subject        string `json:"Subject"`
	Domain         string `json:"Domain"`
}

type FDUpdateHook struct {
	ID             int64  `json:"ID"`
	From           string `json:"From"`
	To             string `json:"To"`
	Status         string `json:"Status"`
	Priority       string `json:"Priority"`
	OfficeLocation string `json:"OfficeLocation"`
	Domain         string `json:"Domain"`
	Cmd            string `json:"Cmd"`
}

type FDConversation struct {
	ID          int64          `json:"id"`
	Body        string         `json:"body"`
	FromEmail   string         `json:"from_email"`
	ToEmails    []string       `json:"to_emails"`
	CcEmails    []string       `json:"cc_emails"`
	FdUserID    int            `json:"user_id"`
	Attachments []FDAttachment `json:"attachments"`
	Visible     bool           `json:"private"`
}

type FDRequestContent struct {
	Body        string
	Cc          []string
	Attachments []OtrsAttachment
}
