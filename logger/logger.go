package logger

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"sync"
)

type (
	programLogger struct {
		level  int
		logger *log.Logger
	}

	logger struct {
		testFlag   bool
		debugLog   programLogger
		infoLog    programLogger
		warningLog programLogger
		errorLog   programLogger
		fatalLog   programLogger
		level      int
		writer     io.Writer
		m          sync.Mutex
	}
)

var programLog *logger

func init() {
	programLog = &logger{
		testFlag: true,
	}
}

func GetLogWriter() io.Writer {
	return programLog.writer
}

func InitLogger(filepath string, logToConsole bool) {
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if logToConsole {
		programLog.writer = io.MultiWriter(file, os.Stdout)
	} else {
		programLog.writer = io.Writer(file)
	}
	if err != nil {
		log.Fatalln("Failed to open log file ", filepath, ":", err)
	}

	programLog.level = 5

	programLog.debugLog.logger = log.New(programLog.writer,
		"DEBUG: ",
		log.Ldate|log.Ltime|log.Lmicroseconds)
	programLog.debugLog.level = 5

	programLog.infoLog.logger = log.New(programLog.writer,
		"INFO: ",
		log.Ldate|log.Ltime)
	programLog.infoLog.level = 4

	programLog.warningLog.logger = log.New(programLog.writer,
		"WARNING: ",
		log.Ldate|log.Ltime)
	programLog.warningLog.level = 3

	programLog.errorLog.logger = log.New(programLog.writer,
		"ERROR: ",
		log.Ldate|log.Ltime)
	programLog.errorLog.level = 2

	programLog.fatalLog.logger = log.New(programLog.writer,
		"FATAL: ",
		log.Ldate|log.Ltime)
	programLog.fatalLog.level = 1
	programLog.testFlag = false
}

func levelCheck(level int) bool {
	if level > 5 || level < 0 {
		return false
	}
	return true
}

func ChangeLevel(level int) error {
	if programLog == nil {
		return nil
	}
	if !levelCheck(level) {
		return errors.New("Level error.")
	}
	programLog.level = level
	return nil
}

func Debug(message string) {
	if programLog.testFlag {
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.debugLog, fn, line, message)
}

func Debugf(formattedMsg string, a ...interface{}) {
	if programLog.testFlag {
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.debugLog, fn, line, fmt.Sprintf(formattedMsg, a...))
}

func Info(message string) {
	if programLog.testFlag {
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.infoLog, fn, line, message)
}

func Infof(formattedMsg string, a ...interface{}) {
	if programLog.testFlag {
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.infoLog, fn, line, fmt.Sprintf(formattedMsg, a...))
}

func Warning(message string) {
	if programLog.testFlag {
		testLogging(message)
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.warningLog, fn, line, message)
}

func Warningf(formattedMsg string, a ...interface{}) {
	if programLog.testFlag {
		testLogging(fmt.Sprintf(formattedMsg, a...))
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.warningLog, fn, line, fmt.Sprintf(formattedMsg, a...))
}

func Error(message string) {
	if programLog.testFlag {
		testLogging(message)
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.errorLog, fn, line, message)
}

func Errorf(formattedMsg string, a ...interface{}) {
	if programLog.testFlag {
		testLogging(fmt.Sprintf(formattedMsg, a...))
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.errorLog, fn, line, fmt.Sprintf(formattedMsg, a...))
}

func Fatal(message string) {
	if programLog.testFlag {
		testLogging(message)
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.fatalLog, fn, line, message)
	os.Exit(1)
}

func Fatalf(formattedMsg string, a ...interface{}) {
	if programLog.testFlag {
		testLogging(fmt.Sprintf(formattedMsg, a...))
		return
	}
	_, fn, line, _ := runtime.Caller(1)
	logging(&programLog.fatalLog, fn, line, fmt.Sprintf(formattedMsg, a...))
	os.Exit(1)
}

func logging(logger *programLogger, fn string, line int, message string) {
	if logger.level <= programLog.level {
		file := filepath.Base(fn)
		logger.logger.Println(file, line, message)
	}
}

func SetLogWriter(writer io.Writer) {
	programLog.m.Lock()
	if writer != nil {
		if !reflect.ValueOf(writer).IsNil() {
			programLog.writer = writer
		}
	}
	programLog.m.Unlock()
}

func testLogging(message string) {
	programLog.m.Lock()
	if programLog.writer != nil {
		if !reflect.ValueOf(programLog.writer).IsNil() {
			programLog.writer.Write([]byte(message))
		}
	}
	programLog.m.Unlock()
}
