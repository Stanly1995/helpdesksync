package logger

import (
	"testing"
)

//тест инициализации логгера
func TestInitLoggerLevel(t *testing.T) {
	InitLogger("testlog.log", true)
	if programLog.level != 5 {
		t.Error("Init error.")
	}
}

func TestChangeLevel(t *testing.T) {
	InitLogger("testlog.log", true)
	ChangeLevel(3)
	if programLog.level != 3 {
		t.Error("ChangeLevel error.")
	}
}
func TestDebug(t *testing.T) {
	InitLogger("testlog.log", true)
	Debug("TEST")
}

func TestInfo(t *testing.T) {
	InitLogger("testlog.log", true)
	Info("TEST")
}

func TestWarning(t *testing.T) {
	InitLogger("testlog.log", true)
	Warning("TEST")
}

func TestError(t *testing.T) {
	InitLogger("testlog.log", true)
	Error("TEST")
}

func TestNotInit(t *testing.T) {
	ChangeLevel(1)
	Debug("TEST")
	Info("TEST")
	Warning("TEST")
	Error("TEST")
}
