package internal

import (
	"bytes"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"helpdesksync/mocks"
	"helpdesksync/types"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHTTPOtrsReplyHandler_Handler(t *testing.T) {
	scenarioTable := []struct {
		message      string
		request      string
		waitResponse string
	}{
		{
			message:      "Invalid request",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc:"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "Old reply",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc":"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"Reply has not been created."}`,
		},
		{
			message:      "FD domain error",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc":"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"FreshDesk domain error"}`,
		},
		{
			message:      "Fd invalid conversation error",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc":"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"internal error"}`,
		},
		{
			message:      "Otrs agent error",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc":"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"otrs error"}`,
		},
		{
			message:      "no errors",
			request:      `{"TicketID":   "999999", "FreshDeskTicketID": "5251", "FreshDeskDomain": "OneStr", "ArticleID":  "123456","Body": "OLOLOLOLOL", "Cc":"Email#1 <arturs@maxiforex.ru>, Email#2 <mail@mail.maxiforex.ru>","Attachments":[{"Content":     "VEVTVA==", "ContentType": "text/plain", "Filename":    "TEST.txt"}]}`,
			waitResponse: `{"Message":"Reply has been created."}`,
		},
	}
	for _, sc := range scenarioTable {
		t.Run(sc.message, func(ti *testing.T) {
			fdAgent := &mocks.IFreshDeskAgent{}
			otrsAgent := &mocks.IOtrsAgent{}
			ticketsStorage := &mocks.ITicketsStorage{}
			testUpdateArticleRequest := types.OtrsUpdateArticleRequest{
				UserLogin: "api",
				Password:  "Qaz54321",
				TicketID:  "999999",
				ArticleID: "123456",
				DynamicField: []types.OtrsDynamicFieldAdd{
					{
						Name:  "ParentArticleID",
						Value: "1",
					},
				},
			}
			switch sc.message {
			case "Old reply":
				ticketsStorage.On("CheckArticleID", "999999", "123456").Return(true)
			case "FD domain error":
				ticketsStorage.On("CheckArticleID", "999999", "123456").Return(false)
				fdAgent.On("CreateReply", mock.Anything).Return(nil, errors.New("FreshDesk domain error"))
			case "Fd invalid conversation error":
				ticketsStorage.On("CheckArticleID", "999999", "123456").Return(false)
				fdAgent.On("CreateReply", mock.Anything).Return([]byte("lel"), nil)
			case "Otrs agent error":
				ticketsStorage.On("CheckArticleID", "999999", "123456").Return(false)
				fdAgent.On("CreateReply", mock.Anything).Return([]byte(`{"id":1}`), nil)
				otrsAgent.On("UpdateArticle", testUpdateArticleRequest).Return(errors.New("otrs error"))
			case "no errors":
				ticketsStorage.On("CheckArticleID", "999999", "123456").Return(false)
				fdAgent.On("CreateReply", mock.Anything).Return([]byte(`{"id":1}`), nil)
				otrsAgent.On("UpdateArticle", testUpdateArticleRequest).Return(nil)
			}
			th := NewHTTPOtrsReplyHandler(fdAgent, otrsAgent, ticketsStorage)
			w := httptest.NewRecorder()
			gin.SetMode("test")
			r := gin.Default()
			r.POST("/otrs/add_reply", th.Handler)
			reqBody := bytes.NewReader([]byte(sc.request))
			req, _ := http.NewRequest("POST", "/otrs/add_reply", reqBody)
			r.ServeHTTP(w, req)
			assert.Equal(ti, sc.waitResponse, w.Body.String())
		})
	}
}
