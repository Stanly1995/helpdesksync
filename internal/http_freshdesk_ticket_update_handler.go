package internal

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/mkideal/pkg/typeconv"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"sort"
	"sync"
)

type HTTPFreshDeskTicketUpdateHandler struct {
	fdAgent   IFreshDeskAgent
	otrsAgent IOtrsAgent
	ts        ITicketsStorage
	m         sync.Mutex
}

func NewHTTPFreshDeskTicketUpdateHandler(fdAgent IFreshDeskAgent, otrsAgent IOtrsAgent, ts ITicketsStorage) *HTTPFreshDeskTicketUpdateHandler {
	return &HTTPFreshDeskTicketUpdateHandler{
		fdAgent:   fdAgent,
		otrsAgent: otrsAgent,
		ts:        ts,
	}
}

func (ftuh *HTTPFreshDeskTicketUpdateHandler) HandlerTicketUpdateOnAgentChange(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("FD request ticket agent changed:\n %s", string(requestBodyBytes))
	var request types.FDUpdateHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	otrsTicketID, err := ftuh.otrsAgent.SearchTicket(request.ID)
	if err != nil {
		err = ftuh.createTicketWithConversation(request)
		if err != nil {
			logger.Error(err.Error())
			c.JSON(http.StatusOK, gin.H{"Message": err})
			return
		}
		c.JSON(http.StatusOK, gin.H{"Message": "Updated"})
		return
	}
	err = ftuh.updateTicketWithConversation(request, otrsTicketID)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Message": "Updated"})
}

func (ftuh *HTTPFreshDeskTicketUpdateHandler) HandlerTicketUpdateOnPropsChange(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("FD request ticket properties changed:\n %s", string(requestBodyBytes))
	var request types.FDUpdateHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	otrsTicketID, err := ftuh.otrsAgent.SearchTicket(request.ID)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "internal error"})
		return
	}
	err = ftuh.otrsAgent.UpdateTicketFields(otrsTicketID, request.Status, request.Priority, request.OfficeLocation)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Message": "Updated"})
}

func (ftuh *HTTPFreshDeskTicketUpdateHandler) createTicketWithConversation(request types.FDUpdateHook) (err error) {
	ticket, err := ftuh.fdAgent.GetTicket(request.ID, request.Domain)
	if err != nil {
		return
	}
	otrsTicketInformation, err := ftuh.otrsAgent.CreateTicket(ticket, request.From, request.To, request.OfficeLocation, request.Domain)
	if err != nil {
		return
	}
	cth.ts.Add(otrsTicketInformation.TicketID, otrsTicketInformation.ArticleID)
	conversation, err := ftuh.fdAgent.GetConversation(request.ID, request.Domain)
	if err != nil {
		return
	}
	ftuh.m.Lock()
	userID, ok := DomainAgents[request.Domain]
	ftuh.m.Unlock()
	if !ok {
		err = errors.New("wrong helpdesk")
		return
	}
	for _, response := range conversation {
		if response.FdUserID == userID {
			continue
		}
		visible := 1
		if !response.Visible {
			visible = 0
		}
		otrsCc := CreateOtrsEmails(response.CcEmails)
		otrsAttachments := CreateOtrsAttachments(response.Attachments)
		addReplyRequest := types.OtrsAddReplyRequest{
			UserLogin:   Conf.otrsLogin,
			Password:    Conf.otrsPassword,
			TicketID:    otrsTicketInformation.TicketID,
			From:        request.From,
			To:          request.To,
			Subject:     ticket.Subject,
			Visible:     visible,
			Body:        response.Body,
			Cc:          otrsCc,
			Attachments: otrsAttachments,
			DynamicField: []types.OtrsDynamicFieldAdd{
				{
					Name:  "ParentArticleID",
					Value: typeconv.ToString(response.ID),
				},
			},
		}
		otrsTicketID, otrsArticleID, err := ftuh.otrsAgent.CreateArticle(addReplyRequest)
		if err != nil {
			logger.Error(err.Error())
			continue
		}
		ftuh.ts.Add(otrsTicketID, otrsArticleID)
	}
	return nil
}

func (ftuh *HTTPFreshDeskTicketUpdateHandler) updateTicketWithConversation(request types.FDUpdateHook, otrsTicketID string) (err error) {
	ticket, err := ftuh.fdAgent.GetTicket(request.ID, request.Domain)
	if err != nil {
		return
	}
	err = ftuh.otrsAgent.UpdateTicketFields(otrsTicketID, request.Status, request.Priority, request.OfficeLocation)
	if err != nil {
		logger.Error(err.Error())
	}
	otrsTicket, err := ftuh.otrsAgent.GetTicket(otrsTicketID)
	if err != nil {
		return
	}
	conversation, err := ftuh.fdAgent.GetConversation(request.ID, request.Domain)
	if err != nil {
		return
	}
	ftuh.m.Lock()
	userID, ok := DomainAgents[request.Domain]
	ftuh.m.Unlock()
	if !ok {
		err = errors.New("Wrong helpdesk domain")
		return
	}
	ftuh.sortConversationByID(conversation)
	for _, response := range conversation {
		if response.FdUserID == userID {
			continue
		}
		check := false
		for _, otrsArticle := range otrsTicket.Article {
			for _, artDF := range otrsArticle.DynamicField {
				if artDF.Name == "ParentArticleID" {
					if artDF.Value == typeconv.ToString(response.ID) {
						check = true
						break
					}
				}
			}
		}
		if !check {
			visible := 0
			if !response.Visible {
				visible = 1
			}
			otrsCc := CreateOtrsEmails(response.CcEmails)
			otrsAttachments := CreateOtrsAttachments(response.Attachments)
			addReplyRequest := types.OtrsAddReplyRequest{
				UserLogin:   Conf.otrsLogin,
				Password:    Conf.otrsPassword,
				TicketID:    otrsTicketID,
				From:        request.From,
				To:          request.To,
				Subject:     ticket.Subject,
				Visible:     visible,
				Body:        response.Body,
				Cc:          otrsCc,
				Attachments: otrsAttachments,
				DynamicField: []types.OtrsDynamicFieldAdd{
					{
						Name:  "ParentArticleID",
						Value: typeconv.ToString(response.ID),
					},
				},
			}
			otrsTicketID, otrsArticleID, err := ftuh.otrsAgent.CreateArticle(addReplyRequest)
			if err != nil {
				logger.Error(err.Error())
				continue
			}
			ftuh.ts.Add(otrsTicketID, otrsArticleID)
		}
	}
	return nil
}

func (ftuh *HTTPFreshDeskTicketUpdateHandler) sortConversationByID(conversation []types.FDConversation) {
	sort.Slice(conversation, func(i, j int) bool {
		return conversation[i].ID < conversation[j].ID
	})
}
