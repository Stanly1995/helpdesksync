package internal

import (
	"bytes"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"helpdesksync/mocks"
	"helpdesksync/types"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewHTTPFreshDeskCreateTicketHandler(t *testing.T) {
	fdAgent := &mocks.IFreshDeskAgent{}
	otrsAgent := &mocks.IOtrsAgent{}
	ticketsStorage := &mocks.ITicketsStorage{}
	fdTh := NewHTTPFreshDeskCreateTicketHandler(fdAgent, otrsAgent, ticketsStorage)
	waitFdTh := &HTTPFreshDeskCreateTicketHandler{
		fdAgent:   fdAgent,
		otrsAgent: otrsAgent,
		ts:        ticketsStorage,
	}
	assert.Equal(t, waitFdTh, fdTh)
}

func TestHTTPFreshDeskCreateTicketHandler_Handler(t *testing.T) {

	scenarioTable := []struct {
		message      string
		request      string
		waitResponse string
	}{
		{
			message:      "No errors",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Domain":"OneStr"}`,
			waitResponse: `{"Message":"Ticket has been created."}`,
		},
		{
			message:      "Invalid json",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Domain":"OneStr}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "Invalid request",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Domain":"OneStr"}`,
			waitResponse: `{"Message":"invalid ticket"}`,
		},
		{
			message:      "OTRS error",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Domain":"OneStr"}`,
			waitResponse: `{"Message":"otrs error"}`,
		},
	}
	for _, sc := range scenarioTable {
		t.Run(sc.message, func(ti *testing.T) {
			fdAgent := &mocks.IFreshDeskAgent{}
			otrsAgent := &mocks.IOtrsAgent{}
			ticketsStorage := &mocks.ITicketsStorage{}
			switch sc.message {
			case "No errors":
				fdAgent.On("GetTicket", int64(5251), "OneStr").Return(types.FDTicket{Id: 5251}, nil)
				otrsAgent.On("CreateTicket", types.FDTicket{Id: 5251}, "freshdesk@email.com", "otrs@email.com", "OneStr").Return(
					types.OtrsSearchTicketResponse{
						TicketID:  "1",
						ArticleID: "1",
					}, nil)
				ticketsStorage.On("Add", "1", "1")
			case "Invalid request":
				fdAgent.On("GetTicket", int64(5251), "OneStr").Return(types.FDTicket{}, errors.New("invalid ticket"))
			case "OTRS error":
				fdAgent.On("GetTicket", int64(5251), "OneStr").Return(types.FDTicket{Id: 5251}, nil)
				otrsAgent.On("CreateTicket", types.FDTicket{Id: 5251}, "freshdesk@email.com", "otrs@email.com", "OneStr").Return(types.OtrsSearchTicketResponse{}, errors.New("otrs error"))
			}
			th := NewHTTPFreshDeskCreateTicketHandler(fdAgent, otrsAgent, ticketsStorage)
			w := httptest.NewRecorder()
			gin.SetMode("test")
			r := gin.Default()
			r.POST("/freshdesk/add_ticket", th.Handler)
			requestBody := bytes.NewReader([]byte(sc.request))
			req, _ := http.NewRequest("POST", "/freshdesk/add_ticket", requestBody)
			r.ServeHTTP(w, req)
			assert.Equal(ti, sc.waitResponse, w.Body.String())
		})
	}
}
