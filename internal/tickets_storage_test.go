package internal

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewTicketsStorage(t *testing.T) {
	checkTs := &TicketsStorage{storage: make(map[string]map[string]struct{})}
	ts := NewTicketsStorage()
	assert.Equal(t, checkTs, ts)
}

func TestTicketsStorage_Add(t *testing.T) {
	checkTs := &TicketsStorage{storage: map[string]map[string]struct{}{
		"1": map[string]struct{}{
			"1": struct{}{},
		},
	}}
	ts := NewTicketsStorage()
	ts.Add("1", "1")
	assert.Equal(t, checkTs, ts)
}

func TestTicketsStorage_CheckArticleID(t *testing.T) {
	scenarioTable := []struct {
		message      string
		inID         string
		inArticleID  string
		waitResponse bool
		waitStorage  map[string]map[string]struct{}
	}{
		{
			message:      "ts hasn't id",
			inID:         "2",
			inArticleID:  "2",
			waitResponse: false,
			waitStorage: map[string]map[string]struct{}{
				"1": {
					"1": {},
				},
			},
		},
		{
			message:      "ts hasn't article",
			inID:         "1",
			inArticleID:  "2",
			waitResponse: false,
			waitStorage: map[string]map[string]struct{}{
				"1": {
					"1": {},
				},
			},
		},
		{
			message:      "ts has id and article",
			inID:         "1",
			inArticleID:  "1",
			waitResponse: true,
			waitStorage: map[string]map[string]struct{}{
				"1": {},
			},
		},
	}
	for _, sc := range scenarioTable {
		t.Run(sc.message, func(ti *testing.T) {
			ts := &TicketsStorage{
				storage: map[string]map[string]struct{}{
					"1": {
						"1": {},
					},
				},
			}
			response := ts.CheckArticleID(sc.inID, sc.inArticleID)
			assert.Equal(ti, sc.waitResponse, response)
			assert.Equal(ti, sc.waitStorage, ts.storage)
		})
	}
}
