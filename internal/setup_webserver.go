package internal

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"helpdesksync/logger"
)

var (
	e    *gin.Engine
	cth  *HTTPFreshDeskCreateTicketHandler
	crh  *HTTPFreshDeskReplyHandler
	orh  *HTTPOtrsReplyHandler
	ftuh *HTTPFreshDeskTicketUpdateHandler
	otuh *HTTPOtrsTicketUpdateHandler
)

func RunWebServer(fdAgent IFreshDeskAgent, otrsAgent IOtrsAgent, ts ITicketsStorage) {
	gin.DefaultWriter = logger.GetLogWriter()
	gin.DefaultErrorWriter = logger.GetLogWriter()
	gin.SetMode(gin.ReleaseMode)
	e = gin.Default()
	cth = NewHTTPFreshDeskCreateTicketHandler(fdAgent, otrsAgent, ts)
	crh = NewHTTPFreshDeskReplyHandler(fdAgent, otrsAgent, ts)
	orh = NewHTTPOtrsReplyHandler(fdAgent, otrsAgent, ts)
	ftuh = NewHTTPFreshDeskTicketUpdateHandler(fdAgent, otrsAgent, ts)
	otuh = NewHTTPOtrsTicketUpdateHandler(fdAgent)
	setupRoutes()

	// run rest server
	hostPort := fmt.Sprintf(":%s", Conf.wsPort)
	err := e.RunTLS(
		hostPort,
		Conf.certCrt,
		Conf.certKey)
	if err != nil {
		logger.Fatalf(`Failed to start Gin Web Server: %s`, err.Error())
	}
	logger.Infof("Gin Web server has been started at :%s", hostPort)
}

func setupRoutes() {
	e.POST("/freshdesk/add_ticket", cth.Handler)
	e.POST("/freshdesk/add_reply", crh.Handler)
	e.POST("/freshdesk/update_agent", ftuh.HandlerTicketUpdateOnAgentChange)
	e.POST("/freshdesk/update_fields", ftuh.HandlerTicketUpdateOnPropsChange)
	e.POST("/otrs/add_reply", orh.Handler)
	e.POST("/otrs/update_ticket", otuh.Handler)
}
