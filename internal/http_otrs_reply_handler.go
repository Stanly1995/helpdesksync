package internal

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/mkideal/pkg/typeconv"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"sync"
)

type HTTPOtrsReplyHandler struct {
	fdAgent   IFreshDeskAgent
	otrsAgent IOtrsAgent
	ts        ITicketsStorage
	m         sync.Mutex
}

func NewHTTPOtrsReplyHandler(fdAgent IFreshDeskAgent, otrsAgent IOtrsAgent, ts ITicketsStorage) *HTTPOtrsReplyHandler {
	return &HTTPOtrsReplyHandler{
		fdAgent:   fdAgent,
		otrsAgent: otrsAgent,
		ts:        ts,
	}
}

func (orh *HTTPOtrsReplyHandler) Handler(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("HTTPOtrsReplyHandler. OTRS sent request: %s \n", string(requestBodyBytes))
	var request types.OtrsReplyHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	if !orh.ts.CheckArticleID(request.TicketID, request.ArticleID) {
		otrsTicket, err := orh.otrsAgent.GetTicket(request.TicketID)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
			return
		}
		if len(otrsTicket.Article) > 0 {
			request.CustomerEmail = otrsTicket.Article[0].From
		} else {
			logger.Warning("OTRS Ticket hasn't articles \n")
		}
		response, err := orh.fdAgent.CreateReply(request)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
			return
		}
		var conv types.FDConversation
		err = json.Unmarshal(response, &conv)
		if err != nil {
			logger.Error(err.Error())
			c.JSON(http.StatusOK, gin.H{"Message": "internal error"})
			return
		}
		updateArticleRequest := types.OtrsUpdateArticleRequest{
			UserLogin: Conf.otrsLogin,
			Password:  Conf.otrsPassword,
			TicketID:  request.TicketID,
			ArticleID: request.ArticleID,
			DynamicField: []types.OtrsDynamicFieldAdd{
				{
					Name:  "ParentArticleID",
					Value: typeconv.ToString(conv.ID),
				},
			},
		}
		err = orh.otrsAgent.UpdateArticle(updateArticleRequest)
		if err != nil {
			logger.Error(err.Error())
			c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"Message": "Reply has been created."})
		if err != nil {
			c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
			return
		}
		return
	}
	logger.Debug("OTRS replied. This is message from FreshDesk")
	c.JSON(http.StatusOK, gin.H{"Message": "Reply has not been created."})
}
