package internal

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"sync"
)

type HTTPFreshDeskCreateTicketHandler struct {
	fdAgent   IFreshDeskAgent
	otrsAgent IOtrsAgent
	ts        ITicketsStorage
	m         sync.Mutex
}

func NewHTTPFreshDeskCreateTicketHandler(fdAgent IFreshDeskAgent, otrsAgent IOtrsAgent, ts ITicketsStorage) *HTTPFreshDeskCreateTicketHandler {
	return &HTTPFreshDeskCreateTicketHandler{
		fdAgent:   fdAgent,
		otrsAgent: otrsAgent,
		ts:        ts,
	}
}

func (cth *HTTPFreshDeskCreateTicketHandler) Handler(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("Create ticket. FD sent request: %s \n", string(requestBodyBytes))
	var request types.FDTicketHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	fdTicket, err := cth.fdAgent.GetTicket(request.ID, request.Domain)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
		return
	}
	otrsResponse, err := cth.otrsAgent.CreateTicket(fdTicket, request.From, request.To, request.OfficeLocation, request.Domain)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "otrs error"})
		return
	}
	cth.ts.Add(otrsResponse.TicketID, otrsResponse.ArticleID)
	c.JSON(http.StatusOK, gin.H{"Message": "Ticket has been created."})
}
