package internal

import (
	"bytes"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"helpdesksync/mocks"
	"helpdesksync/types"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHTTPFreshDeskReplyHandler_Handler(t *testing.T) {
	scenarioTable := []struct {
		message      string
		request      string
		waitResponse string
	}{
		{
			message:      "No errors",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"Reply has been created."}`,
		},
		{
			message:      "No errors",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "OTRS ERROR",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "ArticleCreate. Request error.",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"otrs error"}`,
		},
		{
			message:      "FreshDeskAgent Error",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "EmptyConversation Error",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "DomainAgents Error",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"TwoStr"}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
		{
			message:      "reply from OTRS",
			request:      `{"ID":5251,"From":"freshdesk@email.com","To":"otrs@email.com","Subject":"Test","Reply":"hello", "Domain":"OneStr"}`,
			waitResponse: `{"Message":"invalid request"}`,
		},
	}
	for _, sc := range scenarioTable {
		t.Run(sc.message, func(ti *testing.T) {
			fdAgent := &mocks.IFreshDeskAgent{}
			otrsAgent := &mocks.IOtrsAgent{}
			ticketsStorage := &mocks.ITicketsStorage{}
			DomainAgents = map[string]int{
				"OneStr": 1,
			}
			Conf.otrsLogin = "api"
			Conf.otrsPassword = "Qaz54321"
			addReplyRequest := types.OtrsAddReplyRequest{
				UserLogin: Conf.otrsLogin,
				Password:  Conf.otrsPassword,
				TicketID:  "1",
				From:      "freshdesk@email.com",
				To:        "otrs@email.com",
				Subject:   "Test",
				Body:      "hello",
				Cc:        "",
				DynamicField: []types.OtrsDynamicFieldAdd{
					{
						Name:  "ParentArticleID",
						Value: "0",
					},
				},
			}
			switch sc.message {
			case "No errors":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return([]types.FDConversation{
					{
						Body:     "hello",
						FdUserID: 2,
					},
				}, nil)
				otrsAgent.On("SearchTicket", int64(5251)).Return("1", nil)
				otrsAgent.On("CreateArticle", addReplyRequest).Return("1", "2", nil)
				ticketsStorage.On("Add", "1", "2").Return(false)
			case "FreshDeskAgent Error":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return(nil, errors.New("conversation error"))
			case "OTRS ERROR":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return([]types.FDConversation{
					{
						Body:     "hello",
						FdUserID: 2,
					},
				}, nil)
				otrsAgent.On("SearchTicket", int64(5251)).Return("", errors.New("otrs error"))
			case "ArticleCreate. Request error.":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return([]types.FDConversation{
					{
						Body:     "hello",
						FdUserID: 2,
					},
				}, nil)
				otrsAgent.On("SearchTicket", int64(5251)).Return("1", nil)
				otrsAgent.On("CreateArticle", addReplyRequest).Return("", "", errors.New("otrs error"))
			case "EmptyConversation Error":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return([]types.FDConversation{}, nil)
			case "DomainAgents Error":
				fdAgent.On("GetConversation", int64(5251), "TwoStr").Return([]types.FDConversation{
					{
						Body:     "hello",
						FdUserID: 2,
					},
				}, nil)
			case "reply from OTRS":
				fdAgent.On("GetConversation", int64(5251), "OneStr").Return([]types.FDConversation{
					{
						Body:     "hello",
						FdUserID: 1,
					},
				}, nil)
			}

			th := NewHTTPFreshDeskReplyHandler(fdAgent, otrsAgent, ticketsStorage)
			w := httptest.NewRecorder()
			gin.SetMode("test")
			r := gin.Default()
			r.POST("/freshdesk/add_reply", th.Handler)
			reqBody := bytes.NewReader([]byte(sc.request))
			req, _ := http.NewRequest("POST", "/freshdesk/add_reply", reqBody)
			r.ServeHTTP(w, req)
			assert.Equal(ti, sc.waitResponse, w.Body.String())
		})
	}
}
