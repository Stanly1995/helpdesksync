package internal

import (
	"bytes"
	"fmt"
	"helpdesksync/logger"
	"io/ioutil"
	"net/http"
	"sync"
)

type IFreshdeskRequester interface {
	RequestGet(command string) (responseBody []byte, err error)
	RequestPut(command string, data []byte) (responseBody []byte, err error)
	RequestPost(command string, data []byte, contentType string) (responseBody []byte, err error)
}

type FreshdeskRequester struct {
	httpClient *http.Client
	addr       string
	login      string
	pass       string
	m          sync.Mutex
}

func NewFreshdeskRequester(addr string, login string, pass string) *FreshdeskRequester {
	if addr == "" {
		logger.Fatal("empty addr")
	}
	if login == "" {
		logger.Fatal("empty login")
	}
	if pass == "" {
		logger.Fatal("empty pass")
	}
	return &FreshdeskRequester{
		httpClient: &http.Client{},
		addr:       addr,
		login:      login,
		pass:       pass,
	}
}

func (tr *FreshdeskRequester) RequestGet(command string) (responseBody []byte, err error) {
	tr.m.Lock()
	defer tr.m.Unlock()
	request, err := http.NewRequest("GET", fmt.Sprintf(`%s%s`, tr.addr, command), nil)
	if err != nil {
		return
	}
	request.Header.Set("Content-Type", "application/json")
	request.SetBasicAuth(tr.login, tr.pass)
	response, err := tr.httpClient.Do(request)
	if err != nil {
		return
	}
	responseBody, err = ioutil.ReadAll(response.Body)
	return
}

func (tr *FreshdeskRequester) RequestPut(command string, data []byte) (responseBody []byte, err error) {
	tr.m.Lock()
	defer tr.m.Unlock()
	requestData := bytes.NewReader([]byte(data))
	request, err := http.NewRequest("PUT", fmt.Sprintf(`%s%s`, tr.addr, command), requestData)
	if err != nil {
		return
	}
	request.Header.Set("Content-Type", "application/json")
	request.SetBasicAuth(tr.login, tr.pass)
	response, err := tr.httpClient.Do(request)
	if err != nil {
		return
	}
	responseBody, err = ioutil.ReadAll(response.Body)
	return
}

func (tr *FreshdeskRequester) RequestPost(command string, data []byte, contentType string) (responseBody []byte, err error) {
	tr.m.Lock()
	defer tr.m.Unlock()
	requestData := bytes.NewReader([]byte(data))
	request, err := http.NewRequest("POST", fmt.Sprintf(`%s%s`, tr.addr, command), requestData)
	if err != nil {
		return
	}
	request.Header.Set("Content-Type", contentType)
	request.SetBasicAuth(tr.login, tr.pass)
	response, err := tr.httpClient.Do(request)
	if err != nil {
		return
	}
	responseBody, err = ioutil.ReadAll(response.Body)
	return
}
