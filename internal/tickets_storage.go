package internal

import (
	"helpdesksync/logger"
	"sync"
)

type ITicketsStorage interface {
	Add(id string, ArticleID string)
	CheckArticleID(id string, ArticleID string) bool
}

type TicketsStorage struct {
	storage map[string]map[string]struct{}
	m       sync.Mutex
}

func NewTicketsStorage() *TicketsStorage {
	return &TicketsStorage{storage: make(map[string]map[string]struct{})}
}

func (ts *TicketsStorage) Add(ticketID string, ArticleID string) {
	ts.m.Lock()
	if _, ok := ts.storage[ticketID]; !ok {
		ts.storage[ticketID] = make(map[string]struct{})
	}
	ts.storage[ticketID][ArticleID] = struct{}{}
	logger.Debugf(`Ticket with id %s has map: %v`, ticketID, ts.storage[ticketID])
	logger.Debugf("Ticket storage. Ticket ID: %s, ArticleID: %s. Added. \n", ticketID, ArticleID)
	ts.m.Unlock()
}

func (ts *TicketsStorage) CheckArticleID(ticketID string, ArticleID string) bool {
	ts.m.Lock()
	defer ts.m.Unlock()
	ticket, ok := ts.storage[ticketID]
	if !ok {
		logger.Debugf("Ticket storage. Article for ticket %s with ArticleID %s not found\n ", ticketID, ArticleID)
		return false
	}
	_, ok = ticket[ArticleID]
	if !ok {
		logger.Debugf("Ticket storage. Article for ticket %s with ArticleID %s not found \n", ticketID, ArticleID)
		return false
	}
	delete(ts.storage[ticketID], ArticleID)
	logger.Debugf("Ticket storage. Article for ticket %s with ArticleID %s found \n", ticketID, ArticleID)
	return true
}
