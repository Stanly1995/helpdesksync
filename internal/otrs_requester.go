package internal

import (
	"bytes"
	"fmt"
	"helpdesksync/logger"
	"io/ioutil"
	"net/http"
	"sync"
)

type IOtrsRequester interface {
	RequestPost(command string, body []byte) ([]byte, error)
}

type OtrsRequester struct {
	httpClient *http.Client
	addr       string
	login      string
	pass       string
	m          sync.Mutex
}

func NewOtrsRequester(addr string, login string, pass string) *OtrsRequester {
	if addr == "" {
		logger.Fatal("empty addr")
	}
	if login == "" {
		logger.Fatal("empty login")
	}
	if pass == "" {
		logger.Fatal("empty pass")
	}
	return &OtrsRequester{
		httpClient: &http.Client{},
		addr:       addr,
		login:      login,
		pass:       pass,
	}
}

func (tr *OtrsRequester) RequestPost(command string, body []byte) (responseBody []byte, err error) {
	tr.m.Lock()
	defer tr.m.Unlock()
	requestBody := bytes.NewReader(body)
	request, err := http.NewRequest("POST", fmt.Sprintf(`%s%s`, tr.addr, command), requestBody)
	if err != nil {
		return
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := tr.httpClient.Do(request)
	if err != nil {
		return
	}
	responseBody, err = ioutil.ReadAll(response.Body)
	return
}
