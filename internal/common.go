package internal

import (
	"encoding/base64"
	"fmt"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

func CreateOtrsAttachments(fdAttachments []types.FDAttachment) (otrsAttachments []types.OtrsAttachment) {
	if len(fdAttachments) > 0 {
		for _, fdAttachment := range fdAttachments {
			respBody, err := http.Get(fdAttachment.URL)
			if err != nil {
				logger.Error(err.Error())
				continue
			}
			respContent, err := ioutil.ReadAll(respBody.Body)
			if err != nil {
				logger.Error(err.Error())
				continue
			}
			base64Content := base64.StdEncoding.EncodeToString(respContent)
			otrsAttachments = append(otrsAttachments, types.OtrsAttachment{
				Content:     base64Content,
				ContentType: fdAttachment.ContentType,
				Filename:    fdAttachment.Name,
			})
		}
	}
	return
}

func CreateOtrsEmails(ccFd []string) string {
	ccBuilder := strings.Builder{}
	for _, ticketCc := range ccFd {
		ccBuilder.WriteString(fmt.Sprintf("%s, ", ticketCc))
	}
	cc := ccBuilder.String()
	if cc != "" {
		return string(cc[0 : len(cc)-2])
	}
	return cc
}

func CreateFdEmails(ccOtrs string) []string {
	ccOtrs = strings.Replace(ccOtrs, ",", "", -1)
	reg := regexp.MustCompile(`[\w,\.]+@[\w,\.]+`)
	return reg.FindAllString(ccOtrs, -1)
}
