package internal

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/mkideal/pkg/typeconv"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"sync"
)

type HTTPFreshDeskReplyHandler struct {
	fdAgent   IFreshDeskAgent
	ts        ITicketsStorage
	otrsAgent IOtrsAgent
	m         sync.Mutex
}

func NewHTTPFreshDeskReplyHandler(fdAgent IFreshDeskAgent, otrsAgent IOtrsAgent, ts ITicketsStorage) *HTTPFreshDeskReplyHandler {
	return &HTTPFreshDeskReplyHandler{
		fdAgent:   fdAgent,
		otrsAgent: otrsAgent,
		ts:        ts,
	}
}

func (frh *HTTPFreshDeskReplyHandler) Handler(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("FD replied. FD sent response with ticket: %s \n", string(requestBodyBytes))
	var request types.FDReplyHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	addReplyRequest, err := frh.createOtrsRequest(request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	otrsTicketID, otrsArticleID, err := frh.otrsAgent.CreateArticle(addReplyRequest)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
		return
	}
	frh.ts.Add(otrsTicketID, otrsArticleID)
	err = frh.otrsAgent.UpdateTicketFields(otrsTicketID, "Open", request.Priority, request.OfficeLocation)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Message": "Reply has been created."})
}

func (frh *HTTPFreshDeskReplyHandler) createOtrsRequest(request types.FDReplyHook) (addReplyRequest types.OtrsAddReplyRequest, err error) {
	reply, err := frh.checkReply(request.ID, request.Domain)
	if err != nil {
		return
	}
	otrsTicketID, err := frh.otrsAgent.SearchTicket(request.ID)
	if err != nil {
		return
	}
	fdTo := []string{}
	for _, toEmail := range reply.ToEmails {
		fdTo = append(fdTo, CreateFdEmails(toEmail)...)
	}
	otrsTo := CreateOtrsEmails(fdTo)
	otrsCc := CreateOtrsEmails(reply.CcEmails)
	otrsAttachments := CreateOtrsAttachments(reply.Attachments)
	visible := 1
	if !reply.Visible {
		visible = 0
	}

	addReplyRequest = types.OtrsAddReplyRequest{
		UserLogin:   Conf.otrsLogin,
		Password:    Conf.otrsPassword,
		TicketID:    otrsTicketID,
		From:        request.From,
		To:          otrsTo,
		Subject:     request.Subject,
		Visible:     visible,
		Body:        reply.Body,
		Cc:          otrsCc,
		Attachments: otrsAttachments,
		DynamicField: []types.OtrsDynamicFieldAdd{
			{
				Name:  "ParentArticleID",
				Value: typeconv.ToString(reply.ID),
			},
		},
	}
	return addReplyRequest, nil
}

func (frh *HTTPFreshDeskReplyHandler) checkReply(ticketID int64, domain string) (reply types.FDConversation, err error) {
	conversationsInfo, err := frh.fdAgent.GetConversation(ticketID, domain)
	if err != nil {
		return
	}
	if len(conversationsInfo) == 0 {
		err = errors.New("empty conversations")
		return
	}
	frh.m.Lock()
	userID, ok := DomainAgents[domain]
	frh.m.Unlock()
	if !ok {
		err = errors.New("wrong helpdesk")
		return
	}
	if conversationsInfo[len(conversationsInfo)-1].FdUserID == userID {
		logger.Debugf("FD check reply. OTRS ID: %d. Replier ID: %d.", userID, conversationsInfo[len(conversationsInfo)-1].FdUserID)
		err = errors.New("FD replied. This is reply from Otrs agent \n")
		return
	}
	return conversationsInfo[len(conversationsInfo)-1], nil
}
