package internal

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"helpdesksync/logger"
	"helpdesksync/types"
	"mime/multipart"
	"strings"
	"sync"
)

type IFreshDeskAgent interface {
	GetTicket(id int64, domain string) (fdTicket types.FDTicket, err error)
	UpdateTicket(id int64, domain string, otrsState string, otrsPriority string) (fdTicket types.FDTicket, err error)
	GetConversation(id int64, domain string) (conversation []types.FDConversation, err error)
	CreateReply(request types.OtrsReplyHook) (response []byte, err error)
}

type FreshDeskAgent struct {
	fdRequesters map[string]IFreshdeskRequester
	m            sync.Mutex
}

var OtrsPriorityStringSync = map[string]int8{
	"S3 Minor":    1,
	"S4 Normal":   2,
	"S2 Major":    3,
	"S1 Critical": 4,
}

var OtrsStatusStringSync = map[string]int8{
	"Open":   2,
	"Closed": 5,
}

func NewFreshdeskAgent(fdRequesters map[string]IFreshdeskRequester) *FreshDeskAgent {
	return &FreshDeskAgent{
		fdRequesters: fdRequesters,
	}
}

func (fa *FreshDeskAgent) GetTicket(id int64, domain string) (fdTicket types.FDTicket, err error) {
	fa.m.Lock()
	fdRequester, ok := fa.fdRequesters[domain]
	fa.m.Unlock()
	if !ok {
		err = errors.New("wrong helpdesk")
		return
	}
	freshDeskTicketResponse, err := fdRequester.RequestGet(fmt.Sprintf("tickets/%v", id))
	logger.Debugf("Get ticket. FD sent response with ticket: %s", string(freshDeskTicketResponse))
	if err != nil {
		return
	}
	err = json.Unmarshal(freshDeskTicketResponse, &fdTicket)
	return
}

func (fa *FreshDeskAgent) UpdateTicket(id int64, domain string, otrsState string, otrsPriority string) (fdTicket types.FDTicket, err error) {
	fa.m.Lock()
	fdRequester, ok := fa.fdRequesters[domain]
	fa.m.Unlock()
	if !ok {
		err = errors.New("wrong helpdesk")
		return
	}
	fdStatus, ok := OtrsStatusStringSync[otrsState]
	if !ok {
		fdStatus = 2
	}
	fdPriority, ok := OtrsPriorityStringSync[otrsPriority]
	if !ok {
		fdPriority = 2
	}
	requestData, _ := json.Marshal(struct {
		Status   int8 `json:"status"`
		Priority int8 `json:"priority"`
	}{
		Status:   fdStatus,
		Priority: fdPriority,
	})
	freshDeskTicketResponse, err := fdRequester.RequestPut(fmt.Sprintf("tickets/%v", id), requestData)
	logger.Debugf("Get ticket. FD sent response with ticket: %s", string(freshDeskTicketResponse))
	if err != nil {
		return
	}
	err = json.Unmarshal(freshDeskTicketResponse, &fdTicket)
	return
}

func (fa *FreshDeskAgent) GetConversation(id int64, domain string) (conversation []types.FDConversation, err error) {
	fa.m.Lock()
	fdRequester, ok := fa.fdRequesters[domain]
	fa.m.Unlock()
	if !ok {
		err = errors.New("invalid request")
		return
	}
	ticketInfoJson, err := fdRequester.RequestGet(fmt.Sprintf(`tickets/%d/conversations`, id))
	logger.Debugf("FD replied. FD sent response with conversation: %s", string(ticketInfoJson))
	if err != nil {
		return
	}
	err = json.Unmarshal(ticketInfoJson, &conversation)
	return
}

func (fa *FreshDeskAgent) CreateReply(request types.OtrsReplyHook) (response []byte, err error) {
	fdPostContent := fa.generateFDPostContent(request)
	requestBody, contentType := fa.composeMultipartBody(fdPostContent)
	fa.m.Lock()
	fdRequester, ok := fa.fdRequesters[request.FDDomain]
	fa.m.Unlock()
	if !ok {
		err = errors.New("wrong helpdesk")
		return
	}
	response, err = fdRequester.RequestPost(fmt.Sprintf(`tickets/%s/reply`, request.FDTicketID), requestBody, contentType)
	logger.Info(string(response))
	return
}

func (fa *FreshDeskAgent) generateFDPostContent(request types.OtrsReplyHook) types.FDRequestContent {
	var cc []string
	cc = append(cc, CreateFdEmails(request.To)...)
	cc = append(cc, CreateFdEmails(request.Cc)...)
	var ccWithoutCustomer []string
	for _, email := range cc {
		if email != request.CustomerEmail {
			ccWithoutCustomer = append(ccWithoutCustomer, email)
		}
	}
	return types.FDRequestContent{
		Body:        request.Body,
		Cc:          ccWithoutCustomer,
		Attachments: request.Attachments,
	}
}

func (fa *FreshDeskAgent) composeMultipartBody(fdPostContent types.FDRequestContent) (requestBody []byte, contentType string) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	brBody := strings.Replace(fdPostContent.Body, "\n", "<br>", -1)
	bodyWriter.WriteField("body", brBody)
	for _, cc := range fdPostContent.Cc {
		bodyWriter.WriteField("cc_emails[]", cc)
	}
	for i := range fdPostContent.Attachments {
		fileWriter, err := bodyWriter.CreateFormFile("attachments[]", fdPostContent.Attachments[i].Filename)
		if err != nil {
			logger.Error(err.Error())
			continue
		}
		attachmentContent, err := base64.StdEncoding.DecodeString(fdPostContent.Attachments[i].Content)
		if err != nil {
			logger.Error(err.Error())
			continue
		}
		_, err = fileWriter.Write(attachmentContent)
		if err != nil {
			logger.Error(err.Error())
			continue
		}
	}
	contentType = bodyWriter.FormDataContentType()
	bodyWriter.Close()
	return bodyBuf.Bytes(), contentType
}
