package internal

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"helpdesksync/logger"
	"helpdesksync/types"
	"io/ioutil"
	"net/http"
	"sync"
)

type HTTPOtrsTicketUpdateHandler struct {
	fdAgent IFreshDeskAgent
	m       sync.Mutex
}

func NewHTTPOtrsTicketUpdateHandler(fdAgent IFreshDeskAgent) *HTTPOtrsTicketUpdateHandler {
	return &HTTPOtrsTicketUpdateHandler{
		fdAgent: fdAgent,
	}
}

func (otuh *HTTPOtrsTicketUpdateHandler) Handler(c *gin.Context) {
	requestBodyBytes, _ := ioutil.ReadAll(c.Request.Body)
	logger.Debugf("HTTPOtrsTicketUpdateHandler. OTRS sent request: %s \n", string(requestBodyBytes))
	var request types.OtrsUpdateTicketHook
	err := json.Unmarshal(requestBodyBytes, &request)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "invalid request"})
		return
	}
	_, err = otuh.fdAgent.UpdateTicket(request.FDTicketID, request.FDDomain, request.State, request.Priority)
	if err != nil {
		logger.Error(err.Error())
		c.JSON(http.StatusOK, gin.H{"Message": "internal error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Message": "ticket has been updated"})
}
