package internal

import (
	"crypto/tls"
	"fmt"
	"github.com/creamdog/gonfig"
	"helpdesksync/logger"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type Config struct {
	logPath  string
	logToCon bool
	logLvl   int

	fdOneRequestAddr string
	fdOneLogin       string
	fdOnePassword    string
	fdOneUserID      int

	fdXRequestAddr string
	fdXLogin       string
	fdXPassword    string
	fdXUserID      int

	otrsRequestAddr string
	otrsLogin       string
	otrsPassword    string
	otrsTicketQueue string

	wsHost  string
	wsPort  string
	certCrt string
	certKey string
}

var Conf Config

var DomainAgents map[string]int

func Init() {
	confFile, err := os.Open(ParseArgs(os.Args))
	LogFatalIfErr("Failed to open config file due to", err)
	Conf = GetConfig(confFile)
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	logger.InitLogger(Conf.logPath, Conf.logToCon)
	if err := logger.ChangeLevel(Conf.logLvl); err != nil {
		logger.Errorf("Failed to change logger level to %d", Conf.logLvl)
	}
	fdOneStrRequester := NewFreshdeskRequester(Conf.fdOneRequestAddr, Conf.fdOneLogin, Conf.fdOnePassword)
	logger.Info("fdOneStrRequester has initialized")
	fdXcriticalRequester := NewFreshdeskRequester(Conf.fdXRequestAddr, Conf.fdXLogin, Conf.fdXPassword)
	logger.Info("fdXcriticalRequester has initialized")
	otrsRequester := NewOtrsRequester(Conf.otrsRequestAddr, Conf.otrsLogin, Conf.otrsPassword)
	logger.Info("otrsRequester has initialized")
	DomainAgents = map[string]int{
		"OneStr":    Conf.fdOneUserID,
		"Xcritical": Conf.fdXUserID,
	}
	fdRequesters := map[string]IFreshdeskRequester{
		"OneStr":    fdOneStrRequester,
		"Xcritical": fdXcriticalRequester,
	}
	otrsAgent := NewOtrsTicketAgent(otrsRequester)
	logger.Info("otrsAgent has initialized")
	fdAgent := NewFreshdeskAgent(fdRequesters)
	logger.Info("otrsAgent has initialized")
	ts := NewTicketsStorage()
	RunWebServer(fdAgent, otrsAgent, ts)
}

func GetConfig(reader io.Reader) (conf Config) {
	c, err := gonfig.FromJson(reader)
	LogFatalIfErr("Failed to parse config due to", err)
	conf.logPath, err = c.GetString("logger/filePath", "./helpdesksync.log")
	LogFatalIfErr(`Failed get config key "logger/filePath" due to`, err)
	conf.logToCon, err = c.GetBool("logger/logToCon", true)
	LogFatalIfErr(`Failed get config key "logger/logToCon" due to`, err)
	conf.logLvl, err = c.GetInt("logger/level", 5)
	LogFatalIfErr(`Failed get config key "logger/level" due to`, err)

	conf.fdOneRequestAddr, err = c.GetString("freshDeskOneStr/requestAddr", "https://onestr.freshdesk.com/api/v2/")
	LogFatalIfErr(`Failed get config key "freshDeskOneStr/requestAddr" due to`, err)
	conf.fdOneLogin, err = c.GetString("freshDeskOneStr/login", "otrsapi@maximarkets.org")
	LogFatalIfErr(`Failed get config key "freshDeskOneStr/login" due to`, err)
	conf.fdOnePassword, err = c.GetString("freshDeskOneStr/password", "Qaz54321")
	LogFatalIfErr(`Failed get config key "freshDeskOneStr/password" due to`, err)
	conf.fdOneUserID, err = c.GetInt("freshDeskOneStr/userID", 33013672787)
	LogFatalIfErr(`Failed get config key "freshDeskOneStr/userID" due to`, err)

	conf.fdXRequestAddr, err = c.GetString("freshDeskXCritical/requestAddr", "https://xcritical.freshdesk.com/api/v2/")
	LogFatalIfErr(`Failed get config key "freshDeskXCritical/requestAddr" due to`, err)
	conf.fdXLogin, err = c.GetString("freshDeskXCritical/login", "apiotrs@maximarkets.org")
	LogFatalIfErr(`Failed get config key "freshDeskXCritical/login" due to`, err)
	conf.fdXPassword, err = c.GetString("freshDeskXCritical/password", "Qaz54321")
	LogFatalIfErr(`Failed get config key "freshDeskXCritical/password" due to`, err)
	conf.fdXUserID, err = c.GetInt("freshDeskXCritical/userID", 12022768031)
	LogFatalIfErr(`Failed get config key "freshDeskXCritical/userID" due to`, err)

	conf.otrsRequestAddr, err = c.GetString("otrs/requestAddr", "https://testhd.wizardsdev.com/otrs/nph-genericinterface.pl/Webservice/freshdesk/")
	LogFatalIfErr(`Failed get config key "otrs/requestAddr" due to`, err)
	conf.otrsLogin, err = c.GetString("otrs/login", "api")
	LogFatalIfErr(`Failed get config key "otrs/login" due to`, err)
	conf.otrsPassword, err = c.GetString("otrs/password", "Qaz54321")
	LogFatalIfErr(`Failed get config key "otrs/password" due to`, err)
	conf.otrsTicketQueue, err = c.GetString("otrs/ticketQueue", "Test")
	LogFatalIfErr(`Failed get config key "otrs/ticketQueue" due to`, err)

	conf.wsHost, err = c.GetString("webserver/host", "0.0.0.0")
	LogFatalIfErr(`Failed get config key "webserver/host" due to`, err)
	conf.wsPort, err = c.GetString("webserver/port", "8090")
	LogFatalIfErr(`Failed get config key "webserver/port" due to`, err)
	conf.certCrt, err = c.GetString("cert/crt", "/etc/letsencrypt/live/testhd.wizardsdev.com/fullchain.pem")
	LogFatalIfErr(`Failed get config key "cert/crt" due to`, err)
	conf.certKey, err = c.GetString("cert/key", "/etc/letsencrypt/live/testhd.wizardsdev.com/privkey.pem")
	LogFatalIfErr(`Failed get config key "cert/key" due to`, err)
	return
}

func LogFatalIfErr(msg string, err error) {
	if err != nil {
		logger.Fatalf("%s: %s", msg, err)
	}
}

func ParseArgs(args []string) string {

	if args == nil {
		panic("String Os.Args is nil in manager.Run")
	}

	if len(args) < 3 {
		fmt.Printf("Usage: ./%s [options]\n\n"+
			"\t-c, --conf\tSet path to config\n", filepath.Base(os.Args[0]))
		os.Exit(1)
	}

	if len(args) > 2 {

		if strings.Compare(args[1], "-c") == 0 || strings.Compare(args[1], "--conf") == 0 {
			return args[2]
		}

	}
	fmt.Printf("Usage: ./%s [options]\n\n"+
		"\t-c, --conf\tSet path to config\n", filepath.Base(os.Args[0]))
	os.Exit(1)
	return ""
}
