package internal

import (
	"encoding/json"
	"errors"
	"github.com/mkideal/pkg/typeconv"
	"helpdesksync/logger"
	"helpdesksync/types"
	"sync"
)

type IOtrsAgent interface {
	CreateTicket(fdTicket types.FDTicket, requesterEmail string, responderEmail string, location string, parentDomain string) (otrsResponse types.OtrsSearchTicketResponse, err error)
	SearchTicket(ticketID int64) (string, error)
	CreateArticle(addReplyRequest types.OtrsAddReplyRequest) (otrsTicketID string, otrsArticleID string, err error)
	UpdateTicketFields(id string, status string, priority string, location string) (err error)
	UpdateArticle(updateRequest types.OtrsUpdateArticleRequest) (err error)
	GetTicket(ticketID string) (ticket types.OtrsTicket, err error)
}

type OtrsAgent struct {
	otrsRequester IOtrsRequester
	m             sync.Mutex
}

var FdPriorityIntSync = map[int8]string{
	1: "S3 Minor",
	2: "S4 Normal",
	3: "S2 Major",
	4: "S1 Critical",
}

var FdStatusIntSync = map[int8]string{
	2: "Open",
	3: "Open",
	4: "Open",
	5: "Closed",
}

var FdStatusStrSync = map[string]string{
	"New":      "Open",
	"Open":     "Open",
	"Pending":  "Open",
	"Resolved": "Open",
	"Closed":   "Closed",
}

var FdPriorityStrSync = map[string]string{
	"Low":    "S3 Minor",
	"Medium": "S4 Normal",
	"High":   "S2 Major",
	"Urgent": "S1 Critical",
}

func NewOtrsTicketAgent(otrsRequester IOtrsRequester) *OtrsAgent {
	return &OtrsAgent{
		otrsRequester: otrsRequester,
	}
}

func (oa *OtrsAgent) CreateTicket(fdTicket types.FDTicket, requesterEmail string, responderEmail string, location string, parentDomain string) (otrsResponse types.OtrsSearchTicketResponse, err error) {
	oa.m.Lock()
	priority, ok := FdPriorityIntSync[fdTicket.Priority]
	if !ok {
		logger.Warning("OtrsAgent. \n CreateTicket. Wrong priority.")
		priority = "S3 Minor"
	}
	state, ok := FdStatusIntSync[fdTicket.Status]
	if !ok {
		logger.Warning("OtrsAgent. \n CreateTicket. Wrong status.")
		state = "open"
	}
	oa.m.Unlock()
	otrsAttachments := CreateOtrsAttachments(fdTicket.Attachments)
	otrsCc := CreateOtrsEmails(fdTicket.CcEmails)
	otrsTicket := types.OtrsCreateTicketRequest{
		Ticket: types.OtrsRequestTicket{
			Title:        fdTicket.Subject,
			Queue:        Conf.otrsTicketQueue,
			Lock:         "unlock",
			Type:         "Incident",
			State:        state,
			Priority:     priority,
			Owner:        Conf.otrsLogin,
			CustomerID:   "FreshDesk",
			CustomerUser: requesterEmail,
		},
		Article: types.OtrsRequestArticle{
			From:                 requesterEmail,
			To:                   responderEmail,
			ContentType:          "text/html; charset=UTF8",
			CommunicationChannel: "Email",
			SenderType:           "customer",
			Subject:              fdTicket.Subject,
			Body:                 fdTicket.Description,
			IsVisibleForCustomer: "1",
			Cc:                   otrsCc,
		},
		UserLogin: Conf.otrsLogin,
		Password:  Conf.otrsPassword,
		DynamicField: []types.OtrsDynamicFieldAdd{
			{
				Name:  "ParentTicketID",
				Value: typeconv.ToString(fdTicket.Id),
			},
			{
				Name:  "ParentDomain",
				Value: parentDomain,
			},
			{
				Name:  "ParentOfficeLocation",
				Value: location,
			},
		},
		Attachment: otrsAttachments,
	}
	otrsRequestBody, err := json.Marshal(otrsTicket)
	if err != nil {
		return
	}
	resp, err := oa.otrsRequester.RequestPost("Ticket", otrsRequestBody)
	logger.Debugf("OtrsAgent. Create ticket. OTRS sent response: %s", string(resp))
	if err != nil {
		return
	}
	err = json.Unmarshal(resp, &otrsResponse)
	return
}

func (oa *OtrsAgent) SearchTicket(ticketID int64) (string, error) {
	requestBody, _ := json.Marshal(types.OtrsTicketSearchRequest{
		UserLogin: Conf.otrsLogin,
		Password:  Conf.otrsPassword,
		DynamicField: types.OtrsDynamicFieldSearch{
			Name:   "ParentTicketID",
			Equals: ticketID,
		},
	})
	response, err := oa.otrsRequester.RequestPost("TicketSearch", requestBody)
	if err != nil {
		return "", err
	}
	var otrsTicket struct {
		TicketID []string `json:"TicketID"`
	}
	err = json.Unmarshal(response, &otrsTicket)
	if err != nil {
		logger.Error(err.Error())
		return "", errors.New("invalid otrs ticket")
	}
	if len(otrsTicket.TicketID) == 0 {
		return "", errors.New("the ticket was not found")
	}
	return otrsTicket.TicketID[0], nil
}

func (oa *OtrsAgent) GetTicket(ticketID string) (ticket types.OtrsTicket, err error) {
	requestBody, _ := json.Marshal(types.OtrsTicketGetRequest{
		UserLogin:     Conf.otrsLogin,
		Password:      Conf.otrsPassword,
		TicketID:      ticketID,
		AllArticles:   1,
		DynamicFields: 1,
	})
	response, err := oa.otrsRequester.RequestPost("TicketGet", requestBody)
	if err != nil {
		return
	}
	var otrsTicket types.OtrsGetTicketResponse
	err = json.Unmarshal(response, &otrsTicket)
	if err != nil {
		return
	}
	if len(otrsTicket.Ticket) == 0 {
		err = errors.New("the ticket was not found")
		return
	}
	return otrsTicket.Ticket[0], nil
}

func (oa *OtrsAgent) CreateArticle(addReplyRequest types.OtrsAddReplyRequest) (otrsTicketID string, otrsArticleID string, err error) {
	replyRequestBody, _ := json.Marshal(addReplyRequest)
	responseBytes, err := oa.otrsRequester.RequestPost("ArticleCreate", replyRequestBody)
	if err != nil {
		return "", "", err
	}
	var responseStruct struct {
		TicketID  string `json:"TicketID"`
		ArticleID string `json:"ArticleID"`
	}
	err = json.Unmarshal(responseBytes, &responseStruct)
	if err != nil {
		return "", "", errors.New("invalid otrs response")
	}
	return responseStruct.TicketID, responseStruct.ArticleID, nil
}

func (oa *OtrsAgent) UpdateArticle(updateRequest types.OtrsUpdateArticleRequest) (err error) {
	replyRequestBody, _ := json.Marshal(updateRequest)
	responseBytes, err := oa.otrsRequester.RequestPost("ArticleUpdate", replyRequestBody)
	logger.Debugf("OtrsAgent. \n Update ticket article. \n OTRS sent response: %s \n", string(responseBytes))
	return
}

func (oa *OtrsAgent) UpdateTicketFields(id string, status string, priority string, location string) (err error) {
	oa.m.Lock()
	ticketState, ok := FdStatusStrSync[status]
	if !ok {
		logger.Warning("OtrsAgent. \n UpdateTicketState. Wrong status.")
		ticketState = "open"
	}
	ticketPr, ok := FdPriorityStrSync[priority]
	if !ok {
		logger.Warning("OtrsAgent. \n UpdateTicketPriority. Wrong priority.")
		ticketPr = "S3 Minor"
	}
	oa.m.Unlock()
	otrsTicket := types.OtrsUpdateRequest{
		UserLogin: Conf.otrsLogin,
		Password:  Conf.otrsPassword,
		TicketID:  id,
		Ticket: types.OtrsRequestTicket{
			State:    ticketState,
			Priority: ticketPr,
		},
		DynamicField: []types.OtrsDynamicFieldAdd{
			{
				Name:  "ParentOfficeLocation",
				Value: location,
			},
		},
	}
	otrsRequestBody, err := json.Marshal(otrsTicket)
	if err != nil {
		return
	}
	resp, err := oa.otrsRequester.RequestPost("TicketUpdate", otrsRequestBody)
	logger.Debugf("OtrsAgent. Update ticket state. \n OTRS sent response: %s \n", string(resp))
	return
}

func (oa *OtrsAgent) UpdateTicketLastArticleID(id string, lastID int64) (err error) {
	if lastID == 0 {
		err = errors.New("id = 0")
		return
	}
	otrsTicket := types.OtrsUpdateRequest{
		UserLogin: Conf.otrsLogin,
		Password:  Conf.otrsPassword,
		TicketID:  id,
		DynamicField: []types.OtrsDynamicFieldAdd{
			{
				Name:  "LastArticleID",
				Value: typeconv.ToString(lastID),
			},
		},
	}
	otrsRequestBody, err := json.Marshal(otrsTicket)
	if err != nil {
		return
	}
	resp, err := oa.otrsRequester.RequestPost("TicketUpdate", otrsRequestBody)
	logger.Debugf("Update ticket in OTRS. OTRS sent response: %s", string(resp))
	return
}
