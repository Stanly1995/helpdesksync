# --
# Copyright (C) 2001-2018 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::GenericInterface::Operation::Ticket::CreateArticle;

use strict;
use warnings;
use MIME::Base64;
use JSON;
use Kernel::System::VariableCheck qw( :all );

use parent qw(
    Kernel::GenericInterface::Operation::Common
    Kernel::GenericInterface::Operation::Ticket::Common
);

our $ObjectManagerDisabled = 1;

=head1 NAME

Kernel::GenericInterface::Operation::Ticket::TicketSearch - GenericInterface Ticket Search Operation backend

=head1 PUBLIC INTERFACE

=head2 new()

usually, you want to create an instance of this
by using Kernel::GenericInterface::Operation->new();

=cut

sub new {
    my ( $Type, %Param ) = @_;

    my $Self = {};
    bless( $Self, $Type );

    # check needed objects
    for my $Needed (qw(DebuggerObject WebserviceID)) {
        if ( !$Param{$Needed} ) {
            return {
                Success      => 0,
                ErrorMessage => "Got no $Needed!",
            };
        }

        $Self->{$Needed} = $Param{$Needed};
    }

    # get config for this screen
    #$Self->{Config} = $Kernel::OM->Get('Kernel::Config')->Get('GenericInterface::Operation::TicketSearch');

    return $Self;
}

sub _CheckDynamicField {
    my ( $Self, %Param ) = @_;

    my $DynamicField = $Param{DynamicField};

    # check DynamicField item internally
    for my $Needed (qw(Name Value)) {
        if (
            !defined $DynamicField->{$Needed}
                || ( !IsString( $DynamicField->{$Needed} ) && ref $DynamicField->{$Needed} ne 'ARRAY' )
        )
        {
            return {
                ErrorCode    => 'TicketCreate.MissingParameter',
                ErrorMessage => "TicketCreate: DynamicField->$Needed  parameter is missing!",
            };
        }
    }

    # check DynamicField->Name
    if ( !$Self->ValidateDynamicFieldName( %{$DynamicField} ) ) {
        return {
            ErrorCode    => 'TicketCreate.InvalidParameter',
            ErrorMessage => "TicketCreate: DynamicField->Name parameter is invalid! $DynamicField->{Name}",
        };
    }

    # check DynamicField->Value
    if ( !$Self->ValidateDynamicFieldValue( %{$DynamicField} ) ) {
        return {
            ErrorCode    => 'TicketCreate.InvalidParameter',
            ErrorMessage => "TicketCreate: DynamicField->Value parameter is invalid!",
        };
    }

    # if everything is OK then return Success
    return {
        Success => 1,
    };
}

sub Run {
    my ( $Self, %Param ) = @_;

    my $Result = $Self->Init(
        WebserviceID => $Self->{WebserviceID},
    );

    if ( !$Result->{Success} ) {
        $Self->ReturnError(
            ErrorCode    => 'Webservice.InvalidConfiguration',
            ErrorMessage => $Result->{ErrorMessage},
        );
    }


    # check data - only accept undef or hash ref
    if ( defined $Param{Data} && ref $Param{Data} ne 'HASH' ) {

        return $Self->{DebuggerObject}->Error(
            Summary => 'Got Data but it is not a hash ref in Operation Test backend)!'
        );
    }

    if ( defined $Param{Data} && $Param{Data}->{TestError} ) {

        return {
            Success      => 0,
            ErrorMessage => "Error message for error code: $Param{Data}->{TestError}",
            Data         => {
                ErrorData => $Param{Data}->{ErrorData},
            },
        };
    }

    # copy data
#    my $ReturnData;

    my ( $UserID, $UserType ) = $Self->Auth(
        %Param,
    );

    if ( ref $Param{Data} ne 'HASH' and $UserID eq 0 ) {
        return {
        Success => 0,
	ErrorMessage => "Unauthorized",
        Data => undef,
    	};
    }

    my $DynamicField;
    my @DynamicFieldList;

    if ( defined $Param{Data}->{DynamicField} ) {

        # isolate DynamicField parameter
        $DynamicField = $Param{Data}->{DynamicField};

        # homogenate input to array
        if ( ref $DynamicField eq 'HASH' ) {
            push @DynamicFieldList, $DynamicField;
        }
        else {
            @DynamicFieldList = @{$DynamicField};
        }

        # check DynamicField internal structure
        for my $DynamicFieldItem (@DynamicFieldList) {
            if ( !IsHashRefWithData($DynamicFieldItem) ) {
                return {
                    ErrorCode => 'TicketCreate.InvalidParameter',
                    ErrorMessage =>
                        "TicketCreate: Ticket->DynamicField parameter is invalid!",
                };
            }

            # remove leading and trailing spaces
            for my $Attribute ( sort keys %{$DynamicFieldItem} ) {
                if ( ref $Attribute ne 'HASH' && ref $Attribute ne 'ARRAY' ) {

                    #remove leading spaces
                    $DynamicFieldItem->{$Attribute} =~ s{\A\s+}{};

                    #remove trailing spaces
                    $DynamicFieldItem->{$Attribute} =~ s{\s+\z}{};
                }
            }

            # check DynamicField attribute values
            my $DynamicFieldCheck = $Self->_CheckDynamicField( DynamicField => $DynamicFieldItem );

            if ( !$DynamicFieldCheck->{Success} ) {
                return $Self->ReturnError( %{$DynamicFieldCheck} );
            }
        }
    }

    my $ArticleBackendObject = $Kernel::OM->Get('Kernel::System::Ticket::Article')->BackendForChannel(ChannelName => 'Email');

    my $ArticleID = $ArticleBackendObject->ArticleCreate(
            TicketID         => $Param{Data}{TicketID},
            #ArticleType      => 'email-internal',                        # email-external|email-internal|phone|fax|...
            SenderType       => 'agent',                                # agent|system|customer
            From             =>  $Param{Data}{From},       # not required but useful
            To               =>  $Param{Data}{To}, # not required but useful
            Subject          =>  $Param{Data}{Subject},               # required
            Body             =>  $Param{Data}{Body},                     # required
            Charset          => 'UTF8',
            MimeType         => 'text/html',
            HistoryType      => 'OwnerUpdate',                          # EmailCustomer|Move|AddNote|PriorityUpdate|WebRequestCustomer|...
            HistoryComment   => '%%',
            UserID           => $UserID,
            UnlockOnAway     => 1,                                      # Unlock ticket if owner is away
            Cc               => $Param{Data}{Cc},
	    NoAgentNotify    => 0,
            IsVisibleForCustomer => 1,
        );

    my $Cnt;
    if ( IsArrayRefWithData( $Param{Data}->{Attachments} ) ) {
        my @Attachments = @{ $Param{Data}->{Attachments} };
        for my $Attachment ( @Attachments ) {
            if ( ref($Attachment) eq 'HASH' ) {
                my $Content = decode_base64($Attachment->{Content});
		my $Type = $Attachment->{ContentType};
                my $Name = $Attachment->{Filename};
		$Cnt = "$Content $Type $Name";
		$ArticleBackendObject->ArticleWriteAttachment(
	            Content            => $Content,
        	    ContentType        => $Type,
                    #ContentType        => 'text/html; charset="iso-8859-15"',
        	    Filename           => $Name,
        	    Disposition        => 'attachment', # or 'inline'
        	    ArticleID          => $ArticleID,
        	    UserID             => $UserID,
        	);

           }
        }
    }


    # set dynamic fields (only for object type 'article')
    if ( IsArrayRefWithData($Param{Data}->{DynamicField}) ) {

        for my $DynamicField ( @{$Param{Data}->{DynamicField}} ) {


            my $Result = $Self->SetDynamicFieldValue(
                %{$DynamicField},
                TicketID  => $Param{Data}{TicketID},
                ArticleID => $ArticleID,
                UserID    => $UserID,
            );

            if ( !$Result->{Success} ) {
                my $ErrorMessage =
                    $Result->{ErrorMessage} || "Dynamic Field $DynamicField->{Name} could not be"
                    . " set, please contact the system administrator";


                return {
                    Success      => 0,
                    ErrorMessage => $ErrorMessage,
                };
            }
        }
    }

    return {
        Success => 1,
        Data => {
            ArticleID => $ArticleID,
	    TicketID => $Param{Data}{TicketID},
        },
    };
}

1;
