#!/bin/bash

KEY='~/otrs_srv.key'
HOST='52.28.147.233'
USER='centos'

go test || { echo 'test failed!' ; exit 1; }
echo "Building..."
go build -v -ldflags="-s -w" main.go
echo "Pushing to server..."
scp -i ${KEY} ./main ${USER}@${HOST}:/tmp/helpdesksync
echo "Updating service..."
ssh ${USER}@${HOST} -i ${KEY} -t "sudo systemctl stop helpdesksync.service && sudo cp /tmp/helpdesksync /opt/helpdesksync/ && sudo systemctl start helpdesksync.service"