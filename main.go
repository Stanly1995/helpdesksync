package main

import (
	"helpdesksync/internal"
)

func main() {
	internal.Init()
}
